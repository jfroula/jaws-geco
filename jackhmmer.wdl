workflow jackhmmer_wf {
	meta {
	  version: '0.1.0'
	  author: 'Jeff Froula <jlfroula@lbl.gov>'
	}

    File query
    File reference

    call jackhmmer {
        input: query=query,
               reference=reference
    }

}

## ----------------------------- ##
task jackhmmer {
    File query
    File reference
	String bname = basename(query)

    command {
		output="output.${bname}.txt"
		tblout="sequences.${bname}.txt"
		domtblout="domains.${bname}.txt"
		alignment="alignment.${bname}.txt"

		shifter --image=jfroula/jaws-geco:1.0.0 \
		  jackhmmer -N 5 --cpu=3 --notextw -o $output --tblout $tblout --domtblout $domtblout \
		  -A $alignment ${query} ${reference} -E 1e-6 -incE 1e-6

		  # these options were not found for this version of jackhmmer.
		  # --all --maxfilt=999999 --realign_max=999999 

    }
    output {
        File output_txt="output.${bname}.txt"
		File tblout="sequences.${bname}.txt"
		File domtblout="domains.${bname}.txt"
		File alignment="alignment.${bname}.txt"
    }

}
